package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class StandardParkingBoy implements ParkingBoy{


    public ParkingTicket park(Car car,List<ParkingLot> parkingLots) {
        ParkingLot currentParkingLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst()
                .orElseThrow(()->new RuntimeException("No available position"));
        return currentParkingLot.park(car);
    }
}
