package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy implements ParkingBoy {

    @Override
    public ParkingTicket park(Car car, List<ParkingLot> parkingLots) {
        ParkingLot currentParkingLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparingDouble(ParkingLot::getVacancyRate))
                .orElseThrow(() -> new RuntimeException("No available position"));
        return currentParkingLot.park(car);
    }
}