package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private int capacity;
    Map<ParkingTicket, Car> parkingTicketCarHashMap = new HashMap<>();

    public Map<ParkingTicket, Car> parkingTicketCarHashMap() {
        return parkingTicketCarHashMap;
    }

    public ParkingTicket park(Car car) {
        if (isFull()) {
            throw new RuntimeException("No available position");
        }
        ParkingTicket parkingTicket = new ParkingTicket(car);
        parkingTicketCarHashMap.put(parkingTicket, car);
        return parkingTicket;
    }

    public ParkingLot() {
        this.capacity = 10;
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (isInvalidTicket(parkingTicket)) {
            throw new RuntimeException("Unrecognized parking ticket");
        }
        Car car = parkingTicketCarHashMap.get(parkingTicket);
        parkingTicketCarHashMap.remove(parkingTicket);
        return car;
    }

    public boolean isFull() {
        return parkingTicketCarHashMap.size() >= capacity;
    }

    public boolean isInvalidTicket(ParkingTicket parkingTicket) {
        Car car = parkingTicketCarHashMap.get(parkingTicket);
        return car == null;
    }

    public int getUsedCount() {
        return parkingTicketCarHashMap.size();
    }

    public int getUnusedCount() {
        return capacity - getUsedCount();
    }

    public double getVacancyRate() {
        return 1.0 * getUnusedCount() / capacity;
    }
}
