package com.parkinglot;

import java.util.List;

public class ManageParkingLot {
    private  ParkingBoy parkingBoy;

    private List<ParkingLot> parkingLots;

    public ManageParkingLot(ParkingBoy parkingBoy, List<ParkingLot> parkingLots){
        this.parkingBoy = parkingBoy;
        this.parkingLots = parkingLots;
    }

    public ParkingTicket park(Car car){
        return parkingBoy.park(car,parkingLots);
    }

    public Car fetch(ParkingTicket parkingTicket){
        ParkingLot currentParkingLot = parkingLots.stream()
                .filter(parkingLot -> parkingLot.parkingTicketCarHashMap().containsKey(parkingTicket))
                .findFirst()
                .orElseThrow(()->new RuntimeException("Unrecognized parking ticket"));
        return currentParkingLot.fetch(parkingTicket);
    }
}
