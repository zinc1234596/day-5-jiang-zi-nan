package com.parkinglot;

import java.util.List;

public interface ParkingBoy {
    ParkingTicket park(Car car, List<ParkingLot> parkingLots);
}
