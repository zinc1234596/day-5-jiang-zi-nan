package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy implements ParkingBoy {
    @Override
    public ParkingTicket park(Car car,List<ParkingLot> parkingLots) {
        ParkingLot currentParkingLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparing(ParkingLot::getUnusedCount))
                .orElseThrow(() -> new RuntimeException("No available position"));
        return currentParkingLot.park(car);
    }
}