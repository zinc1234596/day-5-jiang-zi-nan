package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_parking_boy_and_a_car() {
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car("Benz");
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
        ManageParkingLot manageParkingLot = new ManageParkingLot(standardParkingBoy, parkingLotList);
        // when
        ParkingTicket parkingTicket = manageParkingLot.park(car);
        // then
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_the_parked_car_when_fetch_the_car_given_a_parking_boy_with_a_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car("Benz");
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
        ManageParkingLot manageParkingLot = new ManageParkingLot(standardParkingBoy, parkingLotList);
        // when
        ParkingTicket parkingTicket = manageParkingLot.park(car);
        Car parkedCar = manageParkingLot.fetch(parkingTicket);
        // then
        Assertions.assertEquals(car, parkedCar);
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_a_parking_boy_with_two_parked_cars(){
        // given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car1 = new Car("Benz");
        Car car2 = new Car("Tesla");
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
        ManageParkingLot manageParkingLot = new ManageParkingLot(standardParkingBoy, parkingLotList);
        // when
        ParkingTicket parkingTicket1 = manageParkingLot.park(car1);
        ParkingTicket parkingTicket2 = manageParkingLot.park(car2);
        // then
        Assertions.assertEquals(car1, manageParkingLot.fetch(parkingTicket1));
        Assertions.assertEquals(car2, manageParkingLot.fetch(parkingTicket2));
    }

    @Test
    void should_throw_unrecognized_parking_ticket_when_fetch_the_car_given_a_parking_boy_and_a_valid_parking_ticket(){
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car("Benz");
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
        ManageParkingLot manageParkingLot = new ManageParkingLot(standardParkingBoy, parkingLotList);
        // when
        RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
            manageParkingLot.fetch(new ParkingTicket(car));
        });
        // then
        Assertions.assertEquals(runtimeException.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_no_available_position_error_when_park_the_car_given_a_parking_boy_without_any_position(){
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car("Benz");
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
        ManageParkingLot manageParkingLot = new ManageParkingLot(standardParkingBoy, parkingLotList);
        manageParkingLot.park(new Car("Test Car"));
        // when
        RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
            manageParkingLot.park(car);
        });
        Assertions.assertEquals(runtimeException.getMessage(), "No available position");
    }
}
