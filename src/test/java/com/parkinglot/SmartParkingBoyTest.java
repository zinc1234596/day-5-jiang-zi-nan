package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_given_with_two_parking_lots_and_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(3);
        Car car = new Car("Benz");
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(smartParkingBoy, parkingLotList);
        // when
        ParkingTicket parkingTicket = manageParkingLot.park(car);
        // then
        Assertions.assertEquals(car, parkingLot2.fetch(parkingTicket));
    }

    @Test
    void should_return_a_parking_ticket_when_park_given_with_a_full_parking_lot_and_a_parking_lot_and_a_car() {
        // given
        ParkingLot fullParkingLot = new ParkingLot(1);
        ParkingLot parkingLot = new ParkingLot(3);
        Car car = new Car("Benz");
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(fullParkingLot);
        parkingLotList.add(parkingLot);
        ManageParkingLot manageParkingLot = new ManageParkingLot(smartParkingBoy, parkingLotList);
        manageParkingLot.park(new Car("Test Car"));
        // when
        ParkingTicket parkingTicket = manageParkingLot.park(car);
        //then
        Assertions.assertEquals(parkingLot.parkingTicketCarHashMap().get(parkingTicket), car);
    }

    @Test
    void should_return_the_right_car_when_fetch_given_with_two_parking_tickets_and_two_parking_lots() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(3);
        Car car1 = new Car("Toyota");
        Car car2 = new Car("BMW");
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(smartParkingBoy, parkingLotList);
        ParkingTicket parkingTicket1 = manageParkingLot.park(car1);
        ParkingTicket parkingTicket2 = manageParkingLot.park(car2);
        // when
        Car fetchedCar1 = manageParkingLot.fetch(parkingTicket1);
        Car fetchedCar2 = manageParkingLot.fetch(parkingTicket2);
        // then
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_unrecognized_parking_ticket_when_fetch_given_with_an_valid_ticket_and_two_lots() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(3);
        Car car = new Car("BMW");
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(smartParkingBoy, parkingLotList);
        ParkingTicket parkingTicket = manageParkingLot.park(car);
        manageParkingLot.fetch(parkingTicket);
        // when
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class, () -> manageParkingLot.fetch(parkingTicket));
        // then
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_no_available_position_when_park_given_two_full_parking_lot() {
        // given
        ParkingLot fullParkingLot1 = new ParkingLot(1);
        ParkingLot fullParkingLot2 = new ParkingLot(1);
        Car car = new Car("BMW");
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(fullParkingLot1);
        parkingLotList.add(fullParkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(smartParkingBoy, parkingLotList);
        manageParkingLot.park(new Car("Test Car1"));
        manageParkingLot.park(new Car("Test Car2"));
        // when
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class, () -> manageParkingLot.park(car));
        // then
        Assertions.assertEquals("No available position", exception.getMessage());
    }
}