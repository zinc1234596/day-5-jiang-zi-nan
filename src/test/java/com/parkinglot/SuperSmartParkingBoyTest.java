package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoyTest {
    @Test
    void should_park_first_lot_when_park_given_with_two_parking_lots_of_the_same_rate() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(3);
        Car car = new Car("Benz");
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(superSmartParkingBoy, parkingLotList);
        // when
        ParkingTicket parkingTicket = manageParkingLot.park(car);
        // then
        Assertions.assertEquals(parkingLot1.parkingTicketCarHashMap.get(parkingTicket),car);
    }

    @Test
    void should_park_second_lot_when_park_given_with_second_parking_lot_more_rate() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(3);
        Car car = new Car("Benz");
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(superSmartParkingBoy, parkingLotList);
        manageParkingLot.park(car);
        // when
        ParkingTicket parkingTicket = manageParkingLot.park(car);

        // then
        Assertions.assertTrue(parkingLot2.parkingTicketCarHashMap().containsKey(parkingTicket));
    }

    @Test
    void should_return_the_right_car_when_fetch_given_with_two_parking_tickets_and_two_lots() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        Car car1 = new Car("BMW");
        Car car2 = new Car("Benz");
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(superSmartParkingBoy, parkingLotList);
        ParkingTicket parkingTicket1 = manageParkingLot.park(car1);
        ParkingTicket parkingTicket2 = manageParkingLot.park(car2);
        // when
        Car fetchedCar1 = manageParkingLot.fetch(parkingTicket1);
        Car fetchedCar2 = manageParkingLot.fetch(parkingTicket2);
        // then
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_Unrecognized_parking_ticket_when_fetch_given_with_an_unrecognized_ticket_and_two_lots() {
        // given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        Car car = new Car("BMW");
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(superSmartParkingBoy, parkingLotList);
        // when
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> manageParkingLot.fetch(new ParkingTicket(car)));

        // then
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_no_available_position_when_park_given_with_a_car_and_two_full_lots() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        Car car = new Car("BMW");
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ManageParkingLot manageParkingLot = new ManageParkingLot(superSmartParkingBoy, parkingLotList);
        manageParkingLot.park(new Car("Test Car one"));
        manageParkingLot.park(new Car("Test Car two"));
        // when
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> manageParkingLot.park(car));

        // then
        Assertions.assertEquals("No available position", exception.getMessage());
    }
}