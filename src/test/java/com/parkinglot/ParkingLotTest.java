package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_parking_lot_and_a_car(){
        // given
        ParkingLot parkinglot = new ParkingLot();
        Car car = new Car("BMW");
        // when
        ParkingTicket parkingTicket = parkinglot.park(car);
        // then
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void  should_return_the_parked_car_when_fetch_the_car_given_a_parking_lot_with_a_ticket(){
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("BMW");
        // when
        ParkingTicket parkingTicket = parkingLot.park(car1);
        Car car2 = parkingLot.fetch(parkingTicket);
        // then
        Assertions.assertEquals(car1,car2);
    }
    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_a_parking_lot_with_two_parked_cars(){
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("BMW");
        Car car2 = new Car("Tesla");

        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        Assertions.assertEquals(car1,parkingLot.fetch(parkingTicket1));
        Assertions.assertEquals(car2,parkingLot.fetch(parkingTicket2));
    }

    @Test
    void should_throw_unrecognized_parking_ticket_when_fetch_the_car_given_a_parking_lot_and_a_valid_parking_ticket(){
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("ABC");
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        parkingLot.fetch(parkingTicket1);
        RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
            Car verify_car = parkingLot.fetch(parkingTicket1);
        });
        Assertions.assertEquals(runtimeException.getMessage(), "Unrecognized parking ticket");
    }

    @Test
    void should_throw_no_available_position_error_when_park_the_car_given_a_parking_lot_without_any_position(){
        ParkingLot parkingLot = new ParkingLot(0);
        Car car1 = new Car("BMW");
        RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
            ParkingTicket parkingTicket1 = parkingLot.park(car1);
        });
        Assertions.assertEquals(runtimeException.getMessage(), "No available position");
    }
}
