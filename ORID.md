# Daily Report (2023/07/14)

## 1. Key learning, skills and experience acquired

### O:

Today the work involved combining object-oriented programming principles and test-driven development practices to incrementally implement requirements from Story 1 to Story 4.

### R:

Test-driven development provides a valuable way to guide development and ensure code quality. Writing tests first drives the implementation towards meeting requirements.

### I:

A key benefit of TDD is being able to safely refactor code later, knowing that a regression test suite exists to catch any breaks. For ongoing work, running existing tests reduces risks when modifying code.

### D:

For future learning, will continue practicing combining design patterns, TDD and OOP, and verifying through tests.

## 2.Problem / Confusing / Difficulties

The usage of design patterns is still not very familiar. Need more practice applying design patterns appropriately in different scenarios.

## 3. Other Comments / Suggestion

Can spend more time researching design patterns and best practices for combining OOP, TDD and design patterns. This will help improve code quality and flexibility.

